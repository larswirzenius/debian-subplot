# Introduction

How do you know a machine running [Debian](https://debian.org) works,
or at least looks repairable? This is a description minimal
requirements for a working or repairable Debian system. It also
specifies how to verify those requirements are met.

This document is meant to be processed by the
[Subplot](https://subplot.tech) tool, which can produce a test program
to verify the acceptance criteria, based on scenarios specified here.

# Acceptance criteria

This chapter contains requirements and scenarios that verify they are
met. To generate code from the scenarios and run it, use the following
commands:

~~~sh
subplot codegen debian.subplot -o test.py
python3 test.py --env DEBIAN_HOST=xyzzy --env HOME=$HOME --log test.log
~~~

This runs the scenarios against the host `xyzzy`, and writes a log
file to `test.log`. The environment variable `HOME` is copied to the
test program so that it can use your normal SSH configuration.

## Can log in via SSH as the `debian` user and become root with sudo

_Requirement: The system administrator can log in and do things as
root._

The `debian` user is used here. It's the usual user on cloud images
produced by Debian, or that use the Debian version of the `cloud-init`
software.

~~~scenario
given a Debian system
when I run, as debian, sudo id -u
then stdout is exactly "0\n"
~~~

## Can log in and update package lists

_Requirement: The system administrator can update the lists of
packages the APT package manager needs._

Justification: Without this, it's not possible to find out about new
packages, new versions of packages, and there's no hope to apply bug
fixes or security updates. Thus, this is a minimal, if not sufficient,
requirement to be able to maintain a Debian system.

Note that we don't upgrade packages as part of the scenario. That
might break the system.

~~~scenario
given a Debian system
when I run, as debian, sudo apt update
then command is successful
~~~

## Can install and remove a package

_Requirement: The system administrator can install and remove a
package._

Justification: Without this, it may be impossible to install a bug fix
or security update.

The scenario uses the `hello` package, which is rarely used or
installed by anyone. If your Debian system does have it, this will
fail and you will need to edit the scenario to use a different
package.

~~~scenario
given a Debian system
when I try to run, as debian, dpkg -l hello
then command fails
when I run, as debian, sudo apt update
when I run, as debian, sudo apt install -y hello
when I run, as debian, sudo apt remove -y hello
then command is successful
~~~
