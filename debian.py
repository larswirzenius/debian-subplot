import json
import logging
import os
import random
import shlex
import socket
import stat
import yaml


def debian_system(ctx, hostname=None):
    hostname = os.environ["DEBIAN_HOST"]
    logging.debug(f"remember Debian host {hostname}")
    ctx["hostname"] = hostname


def run_as_on_host(ctx, username=None, argv0=None, args=None):
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]
    try_to_run_as_on_host(ctx, username=username, argv0=argv0, args=args)
    runcmd_exit_code_is_zero(ctx)


def try_to_run_as_on_host(ctx, username=None, argv0=None, args=None):
    runcmd_run = globals()["runcmd_run"]

    logging.debug(f"try_to_run_as_on_host: username={username!r}")
    logging.debug(f"try_to_run_as_on_host: argv0={argv0!r}")
    logging.debug(f"try_to_run_as_on_host: args={args!r}")
    target = f"{username}@{ctx['hostname']}"
    argv = ["ssh", target, "--", shlex.quote(argv0)] + shlex.split(args)
    runcmd_run(ctx, argv)
